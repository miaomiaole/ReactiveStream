package org.example.stream;

import java.util.concurrent.Flow;

public class StringSubscriber implements Flow.Subscriber<String> {

    private Flow.Subscription subscription;

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        this.subscription.request(8);
    }

    @Override
    public void onNext(String item) {
        System.out.println(item);
        this.subscription.request(10);
    }

    @Override
    public void onError(Throwable throwable) {
        this.subscription.cancel();
    }

    @Override
    public void onComplete() {
        System.out.println("处理完毕");
    }
}
