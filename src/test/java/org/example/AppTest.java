package org.example;

import org.example.stream.SomeProcessor;
import org.example.stream.SomeSubscriber;
import org.example.stream.StringSubscriber;
import org.junit.Test;

import java.util.Random;
import java.util.concurrent.SubmissionPublisher;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void testReactiveStream() {
        // 发布者
        SubmissionPublisher<Integer> publisher = new SubmissionPublisher();

        // 订阅者
        SomeSubscriber someSubscriber = new SomeSubscriber("订阅者1");
        SomeSubscriber someSubscriber2 = new SomeSubscriber("订阅者2");

        // 创建订阅关系（两个订阅者） 广播形式订阅
        publisher.subscribe(someSubscriber);
        publisher.subscribe(someSubscriber2);

        for (int i = 0; i < 1000; i++) {
            int i1 = new Random().nextInt(100);
            System.out.println("生产者第" + i + "次生产数据" + i1);
            publisher.submit(i1);
        }
        // 消息发送完毕，关闭发布者
        publisher.close();
    }

    @Test
    public void testReactiveProcess() {
        // 发布者
        SubmissionPublisher<Integer> publisher = new SubmissionPublisher();

        // 订阅者
        SomeProcessor someProcessor = new SomeProcessor();
        StringSubscriber stringSubscriber = new StringSubscriber();

        publisher.subscribe(someProcessor);
        someProcessor.subscribe(stringSubscriber);

        for (int i = 0; i < 1000; i++) {
            int i1 = new Random().nextInt(100);
            System.out.println("生产者第" + i + "次生产数据" + i1);
            publisher.submit(i1);
        }
        // 消息发送完毕，关闭发布者
        publisher.close();
    }
}
